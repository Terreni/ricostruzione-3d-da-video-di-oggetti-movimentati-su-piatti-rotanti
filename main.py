import os
import cv2
import numpy as np
import math
import glob
from pyquaternion import Quaternion

def rotate(filename, theta): #Esegue una rotazione intorno all'asse y
    point = []
    color = []
    with open(filename) as fp:
        for cnt, line in enumerate(fp):
            if cnt > 9:
                l = line.split()
                
                r = int(l[3])
                g = int(l[4])
                b = int(l[5])

                arr = np.array([float(l[0]), float(l[1]), float(l[2])])                  
                axis = [0,1,0] 
                
                rotated_v = Quaternion(axis=axis,angle=theta).rotate(arr)
                
                point.append(rotated_v)
                color.append(np.array([r,g,b]))
    return point, color

def calc3dPoint(imgL, imgR, output_file, disparity_map, focal_length):
    mask = disparity_map < 20
    disparity_map[mask] = 20

    h,w = imgR.shape[:2]

    Q2 = np.float32([[1,0,0,0],
                    [0,-1,0,0],
                    [0,0,focal_length*0.05,0], 
                    [0,0,0,1]])
    
    #Reproject points into 3D
    points_3D = cv2.reprojectImageTo3D(disparity_map, Q2)
    #Get color points
    colors = cv2.cvtColor(imgL, cv2.COLOR_BGR2RGB)
    mask_map = disparity_map > 60

    #Mask colors and points. 
    output_points = points_3D[mask_map]
    output_colors = colors[mask_map]

    dx = w / 2
    dy = h / 2

    for i in range(len(output_points)):
        output_points[i][0] = output_points[i][0] - dx
        output_points[i][1] = output_points[i][1] + dy


    create_output(output_points, output_colors, output_file + '.ply')

def computeDepth(imgL,imgR, depthname):
    #Now comes the interesting part, we define the parameters for the SGBM. These parameters can be changed although I recommend to use the below for standard web image sizes:

    # SGBM Parameters -----------------
    window_size = 5#window_size_val                    
    numDispMult = 10
    blockSizeVal = 3
    uniquenessRatioVal = 1
    speckleWindowSizeVal = 1
    left_matcher = cv2.StereoSGBM_create(
        minDisparity = 0,
        numDisparities = numDispMult * 16,            
        blockSize = blockSizeVal,
        P1=8 * 3 * window_size ** 2,  
        P2=32 * 3 * window_size ** 2,
        disp12MaxDiff=1,
        uniquenessRatio=uniquenessRatioVal,
        speckleWindowSize=speckleWindowSizeVal,
        speckleRange=0,
        preFilterCap=2,
        mode=cv2.STEREO_SGBM_MODE_SGBM_3WAY
    )
    #This leads us to define the right_matcher so we can use it for our filtering later. This is a simple one-liner:

    right_matcher = cv2.ximgproc.createRightMatcher(left_matcher)
    #To obtain hole free depth-images we can use the WLS-Filter. This filter also requires some parameters which are shown below:

    # FILTER Parameters
    lmbda = 80000
    sigma = 1.4
    visual_multiplier = 1.0

    wls_filter = cv2.ximgproc.createDisparityWLSFilter(matcher_left=left_matcher)
    wls_filter.setLambda(lmbda)
    wls_filter.setSigmaColor(sigma)
    #Now we can compute the disparities and convert the resulting images to the desired int16 format or how OpenCV names it: CV_16S for our filter:

    #print('computing disparity...')
    displ = left_matcher.compute(imgL, imgR)  # .astype(np.float32)/16
    dispr = right_matcher.compute(imgR, imgL)  # .astype(np.float32)/16
    displ = np.int16(displ)
    dispr = np.int16(dispr)
    #cv2.imwrite(r'depth_out2.jpg', dispr)
    filteredImg = wls_filter.filter(displ, imgL, None, dispr)  # important to put "imgL" here!!!
    #Finally if you show this image with imshow() you may not see anything. This is due to values being not normalized to a 8-bit format. So lets fix this by normalizing our depth map:

    filteredImg = cv2.normalize(src=filteredImg, dst=filteredImg, beta=0, alpha=255, norm_type=cv2.NORM_MINMAX)
    filteredImg = np.uint8(filteredImg)

    filteredImgROTATED = np.flip(filteredImg, 0)
    textureVal = filteredImgROTATED
    filteredImg = cv2.medianBlur(filteredImg,15)
    cv2.imwrite(depthname + '.jpg', filteredImg)
    return filteredImg

def downsample_image(image, reduce_factor):
	for i in range(0,reduce_factor):
		#Check if image is color or grayscale
		if len(image.shape) > 2:
			row,col = image.shape[:2]
		else:
			row,col = image.shape

		image = cv2.pyrDown(image, dstsize= (col//2, row // 2))
	return image


def match_image_pair(img1, img2):
    orb = cv2.ORB_create(nfeatures=2000)

    #orb = cv2.xfeatures2d.SURF_create(400,5,5)
    # find the keypoints and descriptors with SIFT
    kp1, des1 = orb.detectAndCompute(img1, None)
    kp2, des2 = orb.detectAndCompute(img2, None)

    matcher = cv2.DescriptorMatcher_create(cv2.DESCRIPTOR_MATCHER_BRUTEFORCE_HAMMING)
    matches = matcher.match(des1, des2, None)

    # Sort matches by score
    matches.sort(key=lambda x: x.distance, reverse=False)

    # Extract location of good matches
    points1 = np.zeros((len(matches), 2), dtype=np.float32)
    points2 = np.zeros((len(matches), 2), dtype=np.float32)

    for i, match in enumerate(matches):
        points1[i, :] = kp1[match.queryIdx].pt
        points2[i, :] = kp2[match.trainIdx].pt

    M, mask = cv2.findHomography(points1, points2, cv2.RANSAC)
    matchesMask = mask.ravel().tolist()

    matches = [m for m,mask in zip(matches,matchesMask) if mask]
    for i, match in enumerate(matches):
        points1[i, :] = kp1[match.queryIdx].pt
        points2[i, :] = kp2[match.trainIdx].pt
        

    #get_image_pixel(img1, img2, points1, points2)
    print(len(matches))
    #print(img1[int(points1[1,0]), int(points1[1,1])])    
    #print(img2[int(points2[1,0]), int(points2[1,1])])    

    return cv2.drawMatches(img1, kp1, img2, kp2, matches, None), points1, points2
    

def create_output(vertices, colors, filename):
    colors = np.reshape(np.array(colors), (-1,3))
    vertices = np.reshape(np.array(vertices), (-1,3))
    vertices = np.hstack([vertices,colors])

    ply_header = '''ply\nformat ascii 1.0\nelement vertex %(vert_num)d\nproperty float x\nproperty float y\nproperty float z\nproperty uchar red\nproperty uchar green\nproperty uchar blue\nend_header\n'''
    with open(filename, 'w') as f:
        f.write(ply_header %dict(vert_num=len(vertices)))
        np.savetxt(f,vertices,'%f %f %f %d %d %d')

def createFolder(dir_path):
    # Creo la cartella temp
    if not os.path.exists(dir_path + '/temp'):
        os.makedirs(dir_path + '/temp')
    # Creo la cartella temp/depth
    if not os.path.exists(dir_path + '/temp/depth'):
        os.makedirs(dir_path + '/temp/depth')
    # Creo la cartella temp/models
    if not os.path.exists(dir_path + '/temp/models'):
        os.makedirs(dir_path + '/temp/models')
    if not os.path.exists(dir_path + '/temp/homo'):
        os.makedirs(dir_path + '/temp/homo')
    if not os.path.exists(dir_path + '/temp/rotate'):
        os.makedirs(dir_path + '/temp/rotate')

#firstFrame = downsample_image(cv2.imread('und/38.jpg'),2)
#h,w = frame.shape[:2]

dir_path = os.path.dirname(os.path.realpath(__file__))
createFolder(dir_path)

for i in range(38,55, 2):
    firstFrame = downsample_image(cv2.imread(dir_path + '/und/' + str(i) + '.jpg'),2)
    secondFrame = downsample_image(cv2.imread(dir_path + '/und/' + str(i + 1) + '.jpg'),2)
    disparity_map = computeDepth(firstFrame, secondFrame, dir_path + '/temp/depth/depth' + str(i))
    calc3dPoint(firstFrame,secondFrame, dir_path + '/temp/models/model' + str(i), disparity_map, 20)
    
    result_p, result_c = rotate(dir_path + '/temp/models/model' + str(i) + '.ply', 23)

    create_output(result_p, result_c, dir_path + '/temp/rotate/rot '  + str(i) +  '.ply')

for i in range(39, 55, 2):

    img1 = downsample_image(cv2.imread('und/' + str(i) + '.jpg'),2) # queryImage
    img2 = downsample_image(cv2.imread('und/' + str(i+1) + '.jpg'),2) # trainImage

    res,pt1, pt2 = match_image_pair(img1, img2)

    cv2.imwrite(dir_path + '/temp/homo/' + str(i) + str(i+1) + '.jpg',res)


